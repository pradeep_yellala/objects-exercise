


function invert(obj) {
    if (!(Array.isArray(obj)) && !(obj instanceof String) && typeof obj === 'object') {
        object = {}
        for (let key in obj) {
            object[obj[key]] = key
        }
        return object
    } else {
        console.log('please provide an object having key value pair.');
    }
}


module.exports = { invert }