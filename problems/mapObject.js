


function mapObject(obj, cb) {
    if (cb === undefined || typeof cb !== 'function') {
        console.log('please provide function');
        return
    }
    if (!(Array.isArray(obj)) && !(obj instanceof String) && typeof obj === 'object') {
        const arr = []
        for (let i in obj) {
            arr.push(cb(obj[i]))
        }
        return arr
    } else {
        console.log('please provide an object');
    }
}

module.exports = { mapObject }