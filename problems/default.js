


function defaults(obj, defaultProps) {
    if (!(Array.isArray(obj)) && !(obj instanceof String) && typeof obj === 'object' && !(Array.isArray(defaultProps)) && !(defaultProps instanceof String) && typeof defaultProps === 'object') {
        return { ...obj, ...defaultProps }
    } else {
        console.log('Please provide both parameters as key, value pair objects.');
    }
}

module.exports = { defaults }