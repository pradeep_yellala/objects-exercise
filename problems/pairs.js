


function pairs(obj) {
    if (!(Array.isArray(obj)) && !(obj instanceof String) && typeof obj === 'object') {
        const arr = []
        for (let key in obj) {
            arr.push([key, obj[key]]);
        }
        return arr;
    } else {
        console.log('please provide an object');
    }

}

module.exports = { pairs }